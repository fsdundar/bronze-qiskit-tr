# Bronze-Qiskit-Tr için Lisans

## Metin ve Görseller

Metinler ve görseller Creative Commons Attribution 4.0 International Public License (CC-BY-4.0) altında lisanslanmıştır. Lisans metnine şuradan ulaşılabilir: https://creativecommons.org/licenses/by/4.0/legalcode.

## Kod

Çalışma defterlerindeki kod parçaları Apache License 2.0 altında lisanslanmıştır. Lisans metnine şuradan ulaşılabilir: http://www.apache.org/licenses/LICENSE-2.0.
