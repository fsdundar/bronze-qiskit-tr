![](qworld/images/readme-logo.jpg)

## [QWorld](https://qworld.net)'ün Bronze-Qiskit'i

**Bronze** bizim _**kuantum hesaplama ve kuantum programlama**_ üzerine giriş seviyesindeki eğitselimizdir. 2018 Ekim ayında oluşturulmuştur.

**Bronze-Qiskit** Bronze'un [**Qiskit**](https://qiskit.org) kuantum programlama çatısını kullanan versiyonudur. [**IBM Quantum**](https://www.ibm.com/quantum-computing/)'un desteğiyle Şubat 2021 yılında yayınlanmıştır.

Bronze, Jupyter çalışma defterlerinin bir kolleksiyonudur ve her çalışma sayfasında pek çok birebir uygulama imkanı veren programlama örnekleri vardır. Biz Bronze'u kuantum hesaplama ve kuantum programlamanın temellerini yaparak öğrenebileceğiniz bir laboratuvar olarak görüyoruz. Bronze hali hazırda QWorld çatısı altında 50'den fazla çalıştayda ([güncel liste](http://qworld.net/workshop-bronze/#list)) kullanılmıştır. Pedagojik bir strateji olarak eğitselleri basit seviyede tutmak için karmaşık sayıları es geçiyoruz. 

### Önkoşullar

Tek önkoşul programlamanın temellerini (değişkenler ve temel veri yapıları, döngüler ve şartlı ifadeler) bilmektir. Herhangi bir python bilgisi faydalı olacaktır. Eğer böyle bir deneyiminiz yoksa python üzerine çalışma defterlerimize bakabilirsiniz eğitsellere başlamadan önce.

Bronze aynı zamanda temel matematik için de çalışma defterlerine sahiptir. Bunlar vektörler ve matrisler üzerinde temel aritmetik işlemleri konu edinirler.

### Bronze-Qiskit'in bölümleri

- Python (hızlı bir gözden geçirme için)
- Temel matematik (hızlı bir gözden geçirme için)
- Klasik sistemler: bitler, yazı-tura atma, olasılıksal haller ve operatörler, birleşik sistemler, korelasyon, ve kontrollü operatörler
- Qiskit ile Kuantum Sistemler
    - Qiskit'in temelleri: devre tasarımı, görselleştirme, ve simülasyon
    - kuantumun temelleri: kuantum yazı-tura atma ve Hadamard operatörü, kuantum haller ve operatörler, reel-değerli kübitlerin görselleştirilmesi, süperpozisyon ve ölçümler
    - reel-değerli tek kübitler üzerinde operatörler (döndürmeler ve yansıtmalar) ve kuantum tomografi
    - dolanıklık ve temel kuantum protokoller, süperyoğun kodlama ve kuantum teleportasyon
    - Grover'ın arama algoritması

Bunu takip eden giriş seviyesi eğitselimiz (Silver) şuanda gözden geçirme sürecinde. İleri seviye eğitselimiz olacak olan Gold'u 2021 yılında hazırlamaya başlayacağız.

## Katkıda bulunma

Bir _yazım hatası_ ya da _düzeltme_ önermek için lütfen bir pull request ya da issue oluşturun.

_Sorularınız_, _tartışma başlatmak_ ya da _katkı önerisinde bulunmak için_ lütfen bir issue oluşturun.

_Bronze QWorld'ün [QEducation bölümü](https://qworld.net/qeducation/) altında geliştirilmektedir._

## Yükleme

[Yükleme dosyası](installation.pdf)'ndaki adımları takip edin!

<small>

**Binder Kullanımı:** _[Bronze'u bulut ortamında binder ile çalıştırabilirsiniz](https://mybinder.org/v2/gl/qworld%2Fbronze-qiskit/HEAD?urlpath=lab/tree/START.ipynb) fakat **şuna dikkat ediniz ki**_ 
- _her yeni oturum açıldığında başlatması biraz zaman alabilir,_
- _oturum sona erdiğinde değişiklikler kaybolur_
- _eğer 10 dakika içinde yeni bir sekme açılmazsa oturum sona erebilir._
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/qworld%2Fbronze-qiskit/HEAD?urlpath=lab/tree/START.ipynb)
</small>

## Lisans

Metin ve görseller Creative Commons Attribution 4.0 International Public License (CC-BY-4.0) ile lisanslanmıştır. Lisans metnine şuradan ulaşabilirsiniz: https://creativecommons.org/licenses/by/4.0/legalcode.

Çalışma defterlerindeki kod parçacıkları Apache License 2.0 lisansı ile lisanslanmıştır. Lisans metnine şuradan ulaşabilirsiniz: http://www.apache.org/licenses/LICENSE-2.0.

## Teşekkür

HTML dosyalarında (ör. alıştırmalar) matematiksel ifadeleri göstermek için [MathJax](https://www.mathjax.org)'ı kullanıyoruz.

Kuantum yazı-tura atma deneylerini göstermek için açık kaynaklı ve etkileşimli bir araç olan [quantumgame](http://play.quantumgame.io)'i kullanıyoruz.

## Katkıda bulunanlar

Bronze [Abuzer Yakaryilmaz](http://abu.lu.lv) (QWorld & QLatvia) tarafından 2018'in Ekim ayında oluşturulmuştur ve kendisi tarafından geliştirilmekte ve yönetilmektedir.

Özlem Salehi Köken (QWorld & QTurkey) ve Maksims Dimitrijevs (QWorld & QLatvia) diğer katkıda bulunanlardır. Yeni çalışma defterleri oluşturmuşlardır ve var olanları da gözden geçirmişleridir.

Bronze kamuya açık şekilde 7 Temmuz 2019 yılında yayınlanmıştır.

Bronze-Qiskit ise Abuzer Yakaryilmaz tarafından 2021 yılının Şubat ayında yayınlanmıştır.

### Video dersler

Kayıtlı video dersler Abuzer Yakaryilmaz, Özlem Salehi Köken, ve Anastasija Trizna (QLatvia) tarafından 2020'in Ağustos ayında hazırlanmıştır. Yeni kayıtlar 2021'in bahar aylarında yüklenecektir.

### Bronze-Qiskit 2021

Agnieszka Wolska yeni grafikler ve logolar hazırlamıştır.

### Bronze 2020 & 2021

QBronze çalıştaylarına ve [Bronze programı için QTraining](https://qworld.net/qtraining-for-bronze-2020/) katılımcılarına düzeltmeleri ve önerileri için teşekkür ederiz.

### Bronze 2019

[QDrive](https://qworld.net/qdrive/)'ın mentörlerine ve katılımcılarına çok faydalı düzeltmeleri ve önerileri için teşekkür ederiz.

Adam Glos'a (QWorld & QPoland) kendisinin Bronze 2018 üzerine yorumlarından dolayı teşekkür ederiz.

### Bronze 2018

Riga TechGirls'den Katrina Kizenbaha'ya python üzerine olan çalışma defterlerimiz üzerinde yaptığı gözden geçirmeler için teşekkür ederiz.

Martins Kalis'e (QLatvia) python, Qiskit ve çalışma defterlerimiz üzerine teknik yorumları için teşekkür ederiz.

Maksims Dimitrijevs'e (QLatvia) çalışma defterlerimizi dikkatlice okuyup düzeltmeler önerdiği için teşekkür ederiz.

QLatvia'nın ilk üyeleri Martins Kalis, Maksims Dimitrijevs, Aleksejs Naumovs, Andis Draguns, ve Matiss Apinis'e yardım ve destekleri için teşekkür ederiz.

[Faculty of Computing (University of Latvia)](https://www.df.lu.lv)'in 2018 Güz döneminde her Cuma kuantum programlama toplantılarına katılan öğrencilerine çalışma defterlerimizi çalışırkenki yorumları için teşekkür ederiz.

## Tercüme

Bronze-Qiskit'in Türkçe'ye kazandırılması çalışmaları şuanda [Furkan Semih Dündar](https://fsemih.org) tarafından yürütülmektedir ve 2022 yılının Şubat ayında başlamıştır. Tercümeye katkıda bulunmak isteyenler öz geçmişlerini kısa bir motivasyon yazısı ile [bu adrese](mailto:f.semih.dundar@yandex.com) email olarak gönderebilirler.
